<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>REST API</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: black;
                font-family: sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .content {
                text-align: center;
            }

        </style>
    </head>
    <body>
	<h1 class="content">All endpoints for API</h1>
        <ul>
		  <li>GET /api/v1/customer/&lt;customer guid&gt;/accounts  --- List the account ids (guid) of all accounts for the customer</li>
		  <li>GET /api/v1/account/&lt;account guid&gt; --- Return all the details of the account specified</li>
		  <li>GET /api/v1/account/&lt;account guid&gt;/&lt;field&gt; --- Return the field requested for the account specified</li>
		  <li>GET /api/v1/customer/&lt;customer guid&gt;/accounts/?lastname=&lt;lastname&gt; --- Return the account ids for all accounts for the customer with a last name of &lt;lastname&gt;</li>
		  <li>GET /api/v1/customer/&lt;customer guid&gt;/accounts/?firstname=&lt;firstname&gt; --- Return the account ids for all accounts for the customer with a last name of &lt;firstname&gt;</li>
		  <li>GET /api/v1/customer/&lt;customer guid&gt;/accounts/?email_ending=monumentmail.com --- Return the account ids for all accounts for the customer with an email ending @monumentmail.com</li>
		  <li>GET /api/v1/customer/&lt;customer guid&gt;/accounts/?min=2500 --- Return the account ids for all accounts for the customer with a balance higher or equal to 2500</li>
		  <li>GET /api/v1/customer/&lt;customer guid&gt;/accounts/?max=2500 --- Return the account ids for all accounts for the customer with a balance lower or equal to 2500</li>
		  <li>GET /api/v1/customer/&lt;customer guid&gt;/accounts/?min=-2500&amp;max=2000 --- Return the account ids for all accounts for the customer with a balance higher or equal to -2500 and lower or equal to 2000</li>
		  <li>GET /api/v1/customer/&lt;customer guid&gt;/accounts/?&lt;any query&gt;&amp;sort_by=&lt;firstname|lastname|email|telephone|balance&gt; --- Return the account ids for all accounts satisfying query sorted according to parameter sort_by</li>
		</ul>
    </body>
</html>
