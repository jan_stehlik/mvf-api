<?php

namespace App\Http\Middleware;

use Closure;

class RequestAuthentication
{
    /**
     * Authenticate requests.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return response as json  */
    public function handle($request, Closure $next)
    {
		date_default_timezone_set("UTC"); 
		$current_unix_timestamp = time();
		$uri_path = $request->route()->uri();
		$secret_key = "8acfab26-1efb-4d3f-9701-bb4743d62d71";
		
		// get authentication header as a string
		$authentication = $request->header('Authentication');	
		if(!$authentication)
			return response()->json("Access not authenticated!", 401);
			
		// check if message includes "custome-digest" text part
		$text_custom_digest = substr($authentication, 0, 14);
		if($text_custom_digest != "custom-digest ")
			return response()->json("Incorrect authentication message", 403);
			
		// get unix timestamp
		preg_match("/([0-9])+[\b:\b]/", $authentication, $timestamp);
		$unix_timestamp = str_replace(":", "", $timestamp[0]);
		// check if request was sent within the last 15 minutes (900 seconds)
		if(($current_unix_timestamp - 900) > $unix_timestamp)
			return response()->json("Authentication timed out", 403);
			
		// get hashed digest
		preg_match("/[\b:\b][0-9a-zA-Z]+/", $authentication, $hash);
		$hashed_digest = str_replace(":", "", $hash[0]);
		// check if hashed digest is 256 bits long (sha256 hash)
		if(strlen($hashed_digest) != 64)
			return response()->json("Incorrect authentication message", 403);
		
		// GET /api/v1/customer/{customer_guid}/accounts
		if($customer_guid = $request->route('customer_guid')){		
			$uri_path = str_replace("{customer_guid}", $customer_guid, $uri_path);
		}
		// GET /api/v1/account/{account_guid}/{field}
		elseif($field = $request->route('field')){
			$account_guid = $request->route('account_guid');
			
			$uri_path = str_replace("{account_guid}", $account_guid, $uri_path);
			$uri_path = str_replace("{field}", $field, $uri_path);
		}
		// GET /api/v1/account/{account_guid}
		else{
			$account_guid = $request->route('account_guid');
			
			$uri_path = str_replace("{account_guid}", $account_guid, $uri_path);
		}

		$string_to_hash = $unix_timestamp . "GET/" . $uri_path . $secret_key;
		$expected_hashed_digest = hash("sha256", $string_to_hash);

		if($expected_hashed_digest != $hashed_digest)
			return response()->json("Incorrect authentication credentials and/or incorrect uri", 403);
		
        return $next($request);
    }
}
