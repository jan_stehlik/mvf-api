<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;	

class CustomerController extends Controller
{
	/* List the account ids (guid) of all accounts for the customer 
	   @return json
	*/
    public function accounts($customer_guid, Request $request){
		$query = null;
		// filter according to first name
		if(!is_null($firstname = $request->input('firstname'))){
			$query = 'firstname';
			$param_value = $firstname;
		}
		// filter according to last name
		elseif(!is_null($lastname = $request->input('lastname'))){
			$query = 'lastname';
			$param_value = $lastname;
		}
		// filter according to email ending
		elseif(!is_null($email_ending = $request->input('email_ending'))){
			$query = 'email_ending';
			$param_value = $email_ending;
		}
		// filter according to balance range between minimum and maximum
		elseif(!is_null($min_balance = $request->input('min')) && !is_null($max_balance = $request->input('max'))){
			if(!is_numeric($min_balance) || !is_numeric($max_balance))
				return response()->json("Invalid parameter value", 400);
			$query = 'range_balance';
		}
		// filter according to balance with set minimum
		elseif(!is_null($min_balance = $request->input('min'))){
			if(!is_numeric($min_balance))
				return response()->json("Invalid parameter value", 400);
			$query = 'min_balance';
			$param_value = $min_balance;	
		}
		// filter according to balance with set maximum
		elseif(!is_null($max_balance = $request->input('max'))){
			if(!is_numeric($max_balance))
				return response()->json("Invalid parameter value", 400);
			$query = 'max_balance';
			$param_value = $max_balance;
		}
		// sort by parameter
		$sort_by = $request->input('sort_by');

		// download json file
		$ch = curl_init();
		$source = "https://mvf-devtest-s3api.s3-eu-west-1.amazonaws.com/" . $customer_guid . ".json";
		curl_setopt($ch, CURLOPT_URL, $source);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if(!$data = curl_exec($ch))
			return response()->json("Connection error", 503);
		curl_close($ch);
		
		// amazon server returns xml error file if customer_guid does not exist
		if(substr($data, 0, 5) == "<?xml") {
			return json_encode("Customer guid " . $customer_guid . " does not exist");
		}
		
		// return php array
		$json_data = json_decode($data, true);
		
		$accounts_ids = [];
		
		// filter account ids
		foreach($json_data['accounts'] as $acc){
			if($query == 'firstname' || $query == 'lastname'){
				if($acc[$query] == $param_value)
					$accounts_ids[] = $acc;
			}
			elseif($query == 'email_ending'){
				if(strstr($acc['email'], '@'.$param_value))
					$accounts_ids[] = $acc;
			}
			elseif($query == 'range_balance'){
				$balance = str_replace(",", "", $acc['balance']);
				if($balance >= $min_balance && $balance <= $max_balance)	
					$accounts_ids[] = $acc;
			}
			elseif($query == 'min_balance'){
				$balance = str_replace(",", "", $acc['balance']);
				if($balance >= $param_value)
					$accounts_ids[] = $acc;
			}
			elseif($query == 'max_balance'){
				$balance = str_replace(",", "", $acc['balance']);
				if($balance <= $param_value)
					$accounts_ids[] = $acc;
			}
			else
				$accounts_ids[] = $acc;
		}
		// sorting
		if(!is_null($sort_by)){
			// check if specified field is valid
			if($sort_by != "id" && $sort_by != "firstname" && $sort_by != "lastname" && $sort_by != "email" && $sort_by != "telephone" && $sort_by != "balance")
				return response()->json("Field " . $sort_by . " does not exist. Available fields to sort by: id, firstname, lastname, email, telephone, balance.", 400);
			
			$accounts_ids = $this->sort_array($accounts_ids, $sort_by);
		}
		// slicing ids only
		foreach ($accounts_ids as &$element) {
			$element = array_slice($element, 0, 1);
		}
		// reset indexes 		
		$accounts_ids = array_values($accounts_ids);
		
		return json_encode($accounts_ids);
	}
	
	/* Return all the details of the account specified 
	   @return json
	*/
	public function account_details($account_guid){	
		// download xml file about all customers
		$ch = curl_init();
		$source = "https://mvf-devtest-s3api.s3-eu-west-1.amazonaws.com/";
		curl_setopt($ch, CURLOPT_URL, $source);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if(!$data = curl_exec($ch))
			return response()->json("Connection error", 503);
		curl_close($ch);
		
		// convert xml into object
		if(!($xml = simplexml_load_string($data)))
			return response()->json("Error loading xml file", 503);
		
		// loop through all customers and find given account guid
		foreach($xml->Contents as $customer_content){
			$customer_guid = $customer_content->Key;
			// download json file
			$ch = curl_init();
			$source = "https://mvf-devtest-s3api.s3-eu-west-1.amazonaws.com/" . $customer_guid;
			curl_setopt($ch, CURLOPT_URL, $source);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			if(!$data = curl_exec($ch))
				return response()->json("Connection error", 503);
			curl_close($ch);
			
			// return php array
			$json_data = json_decode($data, true);
			
			// filter account ids
			foreach($json_data['accounts'] as $acc){
				if($acc['id'] == $account_guid){					
					return json_encode($acc);
				}
			}
		}
		// Given account guid not found
		return response()->json("Account guid " . $account_guid . " does not exist", 400);
	}
	
	/* Return the field requested for the account specified 
	   @return json
	*/
	public function account_field($account_guid, $field){
		// check if specified field is valid
		if($field != "id" && $field != "firstname" && $field != "lastname" && $field != "email" && $field != "telephone" && $field != "balance")
			return response()->json("Field " . $field . " does not exist. Available fields: id, firstname, lastname, email, telephone, balance.", 400);
		
		// download xml file about all customers
		$ch = curl_init();
		$source = "https://mvf-devtest-s3api.s3-eu-west-1.amazonaws.com/";
		curl_setopt($ch, CURLOPT_URL, $source);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if(!$data = curl_exec($ch))
			return response()->json("Connection error", 503);
		curl_close($ch);
		
		// convert xml into object
		if(!($xml = simplexml_load_string($data)))
			return response()->json("Error loading xml file", 503);
		
		// loop through all customers and find given account guid
		foreach($xml->Contents as $customer_content){
			$customer_guid = $customer_content->Key;
			// download json file
			$ch = curl_init();
			$source = "https://mvf-devtest-s3api.s3-eu-west-1.amazonaws.com/" . $customer_guid;
			curl_setopt($ch, CURLOPT_URL, $source);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			if(!$data = curl_exec($ch))
				return response()->json("Connection error", 503);
			curl_close($ch);
			
			// return php array
			$json_data = json_decode($data, true);
			
			// filter account ids
			foreach($json_data['accounts'] as $acc){
				if($acc['id'] == $account_guid){					
					return json_encode($acc[$field]);
				}
			}
		}
		// Given account guid not found
		return response()->json("Account guid " . $account_guid . " does not exist", 400);
	}
	
	/* Sorting function */
	public function sort_array($array, $on, $order=SORT_ASC)
	{
		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							if($on == 'balance')
								$sortable_array[$k] = str_replace(",", "", $v2);
							else
								$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}
			
			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
				break;
				case SORT_DESC:
					arsort($sortable_array);
				break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}
}
