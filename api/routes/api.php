<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('v1/customer/{customer_guid}/accounts', 'CustomerController@accounts')->name('api.accounts');
Route::get('v1/account/{account_guid}', 'CustomerController@account_details')->name('api.account_details');
Route::get('v1/account/{account_guid}/{field}', 'CustomerController@account_field')->name('api.account_field');
